package clases;

import java.util.ArrayList;

public class GestorContabilidad {
	private ArrayList<Cliente> listaClientes;
	private ArrayList<Factura> listaFacturas;
	
	public GestorContabilidad(){
		listaClientes = new ArrayList<>();
		listaFacturas = new ArrayList<>(); 
	}
	

	//Metodo Temporal para facilitar las pruebas
	public ArrayList<Cliente> getListaClientes() {
		return listaClientes;
	}

	//Metodo Temporal para facilitar las pruebas
	public ArrayList<Factura> getListaFacturas() {
		return listaFacturas;
	}
	
	public Cliente buscarCliente(String dni) {
		for(Cliente unCliente: listaClientes) {
			if(dni.equalsIgnoreCase(unCliente.getDni())) {
				return unCliente;
			}
		}
		return null;
		
	}
	


	public Factura buscarFactura(String códigoFactura) {
		return null;

		
	}
	
	public void altaCliente(Cliente cliente) {
		if(buscarCliente(cliente.getDni())==null) {
			listaClientes.add(cliente);
		}
			
	}
	
	public void crearFactura(Factura factura) {

	}
	
	public Cliente clienteMasAntiguo() {
		return null;
		
	}
	
	public Factura facturaMasCara() {
		return null;
	}
	
	public float calcularFacturacionAnual(int anno) {
		return anno;
		
	}
	
	public void asignarClienteAFactura(String dni, String codigoFactura) {
		
	}
	
	public int cantidadFacturasPorCliente(String dni) {
		return 0;
		
	}
	
	public void eliminarFactura(String código) {
		
	}
	
	public void eliminarCliente(String dni) {
		
	}
	
	public void setListaClientes(ArrayList<Cliente> listaClientes) {
		this.listaClientes = listaClientes;
	}


	public void setListaFacturas(ArrayList<Factura> listaFacturas) {
		this.listaFacturas = listaFacturas;
	}
}
