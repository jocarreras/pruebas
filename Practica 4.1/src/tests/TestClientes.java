package tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDate;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import clases.GestorContabilidad;
import clases.Cliente;

public class TestClientes {

	static GestorContabilidad gestorPruebasClientes;

	Cliente actual;

	@BeforeAll
	/*
	 * Inicializamos el objeto gestorPruebasClientes para poder llamar a los metodos de la clase GestorContabilidad
	 */
	public static void inicializarClase() {
		gestorPruebasClientes = new GestorContabilidad();
	}

	@Test
	/*
	 *Si el objeto que buscamos es diferente de null falla.
	 */
	public void testBuscarClienteInexistenteSinClientes() {
		String dni = "2323464";
		Cliente actual = gestorPruebasClientes.buscarCliente(dni);
		assertNull(actual);
	}

	@Test
	/*
	 * Devuelve el cliente que hemos a�adido
	 */
	public void testBuscarClienteExistente() {
		// busca un cliente que si exista, el resultado debe ser el mismo objeto
		Cliente esperado = new Cliente("Fernando", "1234F", LocalDate.now());
		// A�ado un cliente a la lista de clientes
		gestorPruebasClientes.getListaClientes().add(esperado);
		actual = gestorPruebasClientes.buscarCliente("1234F");
		assertSame(esperado, actual);
	}

	@Test
	/*
	 * Si el objeto que buscamos es diferente de null falla.
	 */
	public void testBuscarClienteInexistenteConClientes() {
		String dni = "64F";
		// Busco un objeto que no exista, el resultado debe ser null
		actual = gestorPruebasClientes.buscarCliente(dni);
		assertNull(actual);
	}

	@Test
	/*
	 * Busca un cliente en la lista con varios clientes
	 */
	public void testBuscarClienteHabiendoVariosClientes() {
		Cliente esperado = new Cliente("Maria", "34567F", LocalDate.parse("1990-05-04"));
		gestorPruebasClientes.getListaClientes().add(esperado);
		String dniABuscar = "1111F";
		Cliente otro = new Cliente("Pedro", dniABuscar, LocalDate.parse("1995-07-02"));
		gestorPruebasClientes.getListaClientes().add(otro);
		actual = gestorPruebasClientes.buscarCliente(dniABuscar);
		assertSame(otro, actual);
	}

	@Test
	/*
	 * A�adir un cliente nuevo a una lista con varios clientes
	 */
	public void testAgregarClienteNuevo() {
		Cliente cliente1 = new Cliente("Joel", "2222H", LocalDate.now());
		gestorPruebasClientes.getListaClientes().add(cliente1);
		Cliente cliente2 = new Cliente("Andrea", "2345K", LocalDate.now());
		gestorPruebasClientes.altaCliente(cliente2);
		Cliente esperado = new Cliente("Andrea", "2345K", LocalDate.now());
		for (int i = 0; i < gestorPruebasClientes.getListaClientes().size(); i++) {
			if (cliente2.getDni().equals(gestorPruebasClientes.getListaClientes().get(i).getDni())) {
				esperado = gestorPruebasClientes.getListaClientes().get(i);
			}
		}
		assertSame(esperado, cliente2);
	}

	@Test
	public void testAgregarClienteRepetido() {
		Cliente cliente1 = new Cliente("Jose", "1223J", LocalDate.now());
		gestorPruebasClientes.getListaClientes().add(cliente1);
		Cliente cliente2 = new Cliente("Pepe", "1234K", LocalDate.now());
		gestorPruebasClientes.altaCliente(cliente2);
		Cliente esperado = new Cliente("Pepe", "1234K", LocalDate.now());
		for (int i = 0; i < gestorPruebasClientes.getListaClientes().size(); i++) {
			if (cliente2.getDni().equals(gestorPruebasClientes.getListaClientes().get(i).getDni())) {
				esperado = gestorPruebasClientes.getListaClientes().get(i);
			}
		}
		assertNotEquals(esperado, cliente2);
	}

	@Test
	/*
	 * elimina un cliente de la lista
	 */
	public void testEliminarCliente() {
		Cliente cliente = new Cliente("aaaaa", "111K", LocalDate.now());
		gestorPruebasClientes.getListaClientes().add(cliente);
		gestorPruebasClientes.eliminarCliente("aaaaa");
		boolean error = false;
		error = gestorPruebasClientes.getListaClientes().contains(cliente);
		assertTrue(error);
	}

	@Test
	/*
	 * elimina un cliente que no existe en la lista
	 */
	public void testEliminarClienteVacio() {
		Cliente cliente = new Cliente("bbbb", "11111O", LocalDate.now());
		gestorPruebasClientes.getListaClientes().add(cliente);
		gestorPruebasClientes.eliminarCliente("bbbb");
	}
	
	@Test
	/*
	 * devuelve el cliente mas antiguo de la lista comparando los 2 que a�adimos
	 */
	public void testClienteMasAntiguo(){
		Cliente cliente1 = new Cliente("Fernando", "1111M", LocalDate.parse("2018-02-11"));
		Cliente cliente2 = new Cliente("Jose", "2222K", LocalDate.parse("1995-08-12"));
		gestorPruebasClientes.getListaClientes().add(cliente1);
		gestorPruebasClientes.getListaClientes().add(cliente2);
		Cliente esperado;
		esperado=gestorPruebasClientes.clienteMasAntiguo();
		assertSame(cliente2, esperado);
	}

	
	
}
