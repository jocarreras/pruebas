package tests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDate;

import org.junit.Assume;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import clases.Cliente;
import clases.Factura;
import clases.GestorContabilidad;

public class TestFacturas {

	static GestorContabilidad gestorPruebasFacturas;
	Factura actual;

	@BeforeAll
	/*
	 * Inicializamos el objeto gestorPruebasClientes para poder llamar a los metodos
	 * de la clase GestorContabilidad
	 */
	public static void inicializarClase() {
		gestorPruebasFacturas = new GestorContabilidad();
	}

	@Test
	/*
	 * Si el objeto que buscamos es diferente de null falla.
	 */
	public void buscarFacturaInexistente() {
		String factura = "234235432";
		actual = gestorPruebasFacturas.buscarFactura(factura);

		assertNull(actual);
	}

	@Test
	/*
	 * Devuelve el objeto que añadimos a la lista
	 */
	public void buscarFacturaExistente() {
		Factura esperada = new Factura("1111", LocalDate.parse("1990-05-04"), "madera", 50, 100);
		gestorPruebasFacturas.getListaFacturas().add(esperada);
		Factura actual = gestorPruebasFacturas.buscarFactura("1111");
		assertSame(esperada, actual);

	}

	@Test
	/*
	 * Añade una factura a la lista
	 */
	public void añadirFactura() {
		Factura factura1 = new Factura("1111", LocalDate.parse("1990-05-04"), "madera", 50, 100);
		gestorPruebasFacturas.getListaFacturas().add(factura1);
		Factura factura2 = new Factura("1234", LocalDate.now(), "jabon", 50, 100);
		gestorPruebasFacturas.crearFactura(factura1);
		Factura esperado = new Factura("1234", LocalDate.now(), "jabon", 50, 100);
		for (int i = 0; i < gestorPruebasFacturas.getListaFacturas().size(); i++) {
			if (factura2.getCodigoFactura()
					.equals(gestorPruebasFacturas.getListaFacturas().get(i).getCodigoFactura())) {
				esperado = gestorPruebasFacturas.getListaFacturas().get(i);
			}
		}
		assertEquals(esperado, factura2);
	}

	@Test
	public void añadirFacturaExistente() {
		Factura factura1 = new Factura("1111", LocalDate.parse("1990-05-04"), "madera", 50, 100);
		gestorPruebasFacturas.getListaFacturas().add(factura1);
		Factura factura2 = new Factura("1234", LocalDate.now(), "jabon", 50, 100);
		gestorPruebasFacturas.crearFactura(factura1);
		Factura esperado = new Factura("1234", LocalDate.now(), "jabon", 50, 100);
		for (int i = 0; i < gestorPruebasFacturas.getListaFacturas().size(); i++) {
			if (factura2.getCodigoFactura()
					.equals(gestorPruebasFacturas.getListaFacturas().get(i).getCodigoFactura())) {
				esperado = gestorPruebasFacturas.getListaFacturas().get(i);
			}
		}
		assertNotEquals(esperado, factura2);
	}

	@Test
	/*
	 * Falla si no devuelve la factura mas cara
	 */
	public void testFacturaMasCara() {
		Factura factura1 = new Factura("1111", LocalDate.parse("1990-05-04"), "cristal", 50, 100);
		Factura factura2 = new Factura("2222", LocalDate.parse("1990-05-04"), "hierro", 60, 100);
		gestorPruebasFacturas.getListaFacturas().add(factura1);
		gestorPruebasFacturas.getListaFacturas().add(factura2);
		Factura esperado;
		esperado = gestorPruebasFacturas.facturaMasCara();
		assertSame(factura2, esperado);
	}

	@Test
	/*
	 * Falla si es diferente de null, por lo tanto al estar vacia nunca fallara ya
	 * que siempre sera null
	 */
	public void testFacturaMasCaraVacia() {
		Factura resultado;
		resultado = gestorPruebasFacturas.facturaMasCara();
		assertNull(resultado);
	}

	@Test
	public void FacturaAnual() {

		Factura factura1 = new Factura("1111", LocalDate.parse("1975-05-04"), "cristal", 50, 100);
		Factura factura2 = new Factura("1111", LocalDate.parse("1975-05-04"), "cristal", 50, 100);
		gestorPruebasFacturas.getListaFacturas().add(factura1);
		gestorPruebasFacturas.getListaFacturas().add(factura2);
		float resultado;
		resultado = gestorPruebasFacturas.calcularFacturacionAnual(LocalDate.parse("1975"));

	}

	
	@Test
	/*
	 * Añade un cliente a una factura
	 */
	public void testAsignarClienteAFactura() {
		Cliente cliente1 = new Cliente("1111P", "Joel", LocalDate.now());
		gestorPruebasFacturas.asignarClienteAFactura("1111P", "Joel");
	}
	
	@Test
	/*
	 * elimina una factura
	 */
	public void eliminarFacturaExistente() {
		Factura factura1 = new Factura("1111", LocalDate.parse("1990-05-04"), "cristal", 50, 100);
		gestorPruebasFacturas.getListaFacturas().add(factura1);
		gestorPruebasFacturas.eliminarFactura("1111");
		boolean error = false;
		error = gestorPruebasFacturas.getListaFacturas().contains(factura1);
		assertTrue(error);
	}

	@Test
	/*
	 * elimina un factura que no exista en la lista
	 */
	public void testEliminarFacturaVacia() {
		Factura factura1 = new Factura("1111", LocalDate.parse("1990-05-04"), "cristal", 50, 100);
		gestorPruebasFacturas.getListaFacturas().add(factura1);
		gestorPruebasFacturas.eliminarFactura("1111");
	}



}
